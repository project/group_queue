<?php

/**
 * @file
 * Implements the GroupQueue class.
 */

class GroupQueue implements DrupalQueueInterface {

  protected $queue_group;

  public function __construct($name) {

    /*
     * Read all configured grouped queues and copy the $name queue
     * configuration into $queue_group_config.
     */
    $var = variable_get('group_queues', array());
    $queue_group_config = $var[$name];

    /*
     * Invoke all hook_cron_queue_info() hooks to build our private queue_info
     * array containing only our group queue's child queues.
     * @todo: Cache this info to avoid triggering all the hook_cron_queue_info
     *        hooks on each item using drupal_static().
     */
    $queues = module_invoke_all('cron_queue_info');
    drupal_alter('cron_queue_info', $queues);
    foreach ($queue_group_config as $queue_name)
    {
      if (array_key_exists($queue_name, $queues)) {
        $this->queue_group[$queue_name] = $queues[$queue_name];
      }
    }

  }

  public function numberOfItems() {
    // @todo: Add a numberOfItems($queue_name) method parameter?
    $total_size = 0;
    foreach ($this->queue_group as $queue_name => $queue_info)
    {
      $objQueue = DrupalQueue::get($queue_name);
      $total_size += $objQueue->numberOfItems();
    }
    return $total_size;
  }

  public function claimItem($lease_time = 30) {

    foreach ($this->queue_group as $queue_name => $queue_info)
    {
      $objQueue = DrupalQueue::get($queue_name);
      $item = $objQueue->claimItem();
      if (!$item) {
        // If no more items in this queue, proceed to next queue in the group.
        continue;
      }

      $worker_callback = $queue_info['worker callback'];
      /*
       * Append queue info to item object properties to carry the
       * original queue properties' along later method calls.
       */
      $item->queue_name = $queue_name;
      $item->worker_callback = $worker_callback;

      /*
       * Add queue info to item's data payload to allow our module's
       * worker callback wrapping the grup's original queues to forward the
       * call to the appropiate queue worker callback.
       */
      $item->data['group_queue'] = array(
        'item_id' => $item->item_id,
        'queue_name' => $queue_name,
        'worker_callback' => $worker_callback,
      );
      return $item;
    }
  }

  public function createItem($data){
    /*
     * This queue backend does not accept enqueueing items, since it can't
     * determine which child queue to place the items on.
     */
    return FALSE;
  }

  public function deleteItem($item) {
    $objQueue = DrupalQueue::get($item->queue_name);
    $objQueue->deleteItem($item);
  }

  public function releaseItem($item) {
    $objQueue = DrupalQueue::get($item->queue_name);
    return $objQueue->releaseItem($item);
  }

  public function createQueue(){}

  public function deleteQueue(){}

}
